## 游戏输入

### 1、获取鼠标事件
- 0 左键 1 右键 2 中键
```C#
// 1. 当鼠标按住左键时
Input.GetMouseButton(0);
// 2. 当鼠标按下左键时
Input.GetMouseButtonDown(0);
// 3. 当鼠标抬起左键时
Input.GetMouseButtonUp(0);
```

### 2、获取键盘事件

```C#
// 1. 当按住 A 时
Input.GetKey(KeyCode.A);
Input.GetKey("a");
// 2. 当按下 A 时
Input.GetKeyDown(KeyCode.A);
// 3. 当抬起 A 时
Input.GetKeyUp(KeyCode.A);
```

