## Unity Animator 动画

### 1、基本介绍
1. 控制多个动画播放，即动画控制器，也称动画状态机
2. 基于动画剪辑 AnimationClip 的系统
3. 动画是基于游戏对象 GameObject

### 2、核心点
1. 需要带有动画组件的模型文件
2. 需要有模型的动画
3. 创建动画控制器

### 3、关键点
#### 1. 熟悉动画控制器的创建
- 路径：Assets>鼠标右键>Create>Animator Controller

### 4. 难点
#### 1. 动画刻录
1. 路径：选中物体>Command+6>创建.anim文件
2. 白线定位关键帧，改变当前物体的属性或状态，形成动画
3. 可以动画的属性：物体位置、网格、碰撞体、脚本属性、子物体的属性

#### 2. 了解动画的组成缺一不可
1. 动画控制器，Animator Controller
2. 角色模型
3. 动画切片，图标为 三角形播放 标志

#### 3. 绑定 Controller
- 动画控制器与物体的动画组件绑定

### 5、运用流程
#### 1. 新建 Animator Controller
1. 路径：Assets>鼠标右键>Create>Animator Controller

#### 2. 添加动画到编辑栏中
1. 双击 Animator Controller
2. 新建 [New State]，增加 Motion 属性值，即选择动画切片
3. 连接 State，并设置参数名称与参数值

#### 3. 给物体添加 Animator 组件
- 绑定 Controller：将制作好的动画控制器与物体的动画组件 Animator 的 Controller 属性绑定
1. 如果物体已有 Animator 组件，则把 Animator Controller 拖到 物体的 Animator>Controller 上
2. 如果物体没有 Animator 组件，则直接把 Animator Controller 拖到 Inspector 中即可

#### 4. 点击播放

### 6、C# 调用动画组件
- 特别注意：一个物体只能有一个动画组件
```C#
this._animator = this.GetComponent<Animator>();
// 更改状态标识，达到更改动画的目的
this._animator.SetInteger("is_int", 1);
// .GetInteger("is_int"); // 获取参数值
this._animator.SetFloat("is_float", 1f);
// .GetFloat("is_int"); // 获取参数值
this._animator.SetBool("is_bool", true);
// .GetBool("is_int"); // 获取参数值
// Trigger 在更改后自动恢复初始值，返回值：true/false
this._animator.SetTrigger("is_trigger");
```

### 7、Layers 层的使用
1. 设置动画分层 Layers 的 weight 权重
2. 无需使用代码控制分层，直接调用动画状态机即可

### 8、Unity 中动画的术语
1. Layers：动画分层，用于一次事件执行多个动画
2. Parameters：动画参数，控制状态的转变
3. 参数值比较：Greater 大于、Less 小于、Equals 相等、NotEqual 不相等

### 附录其他
#### 1. 如何将 PMX 文件转化为 FBX 文件？
