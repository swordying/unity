## Unity UI

1. UI 组件必须处于 Canvas 下，才能生效

### 1、Text
#### 1. 简介
1. 在 Scene 中新建一个文本组件

#### 2. 代码控制 Text

```C#
using UnityEngine.UI;

public Text t;
public Font f;

t = this.transform.Find("文本框路径").GetComponent<Text>();
// t = this.GetComponent<Text>();
// 更新文本值
t.text = "UI-Text 文本";
// 更新文字字体
t.font = f;
// 更新文字大小
t.fontSize = 14;
```

#### 3、更改文本框的背景
1. 添加 Image 组件，然后调整顺序即可
2. 或者为父级节点添加 Image UI 组件

### 2、Text-TextMeshPro

#### 1. 简介
1. 可以创建带有特效(描边|阴影|发光等)的文本框
2. 避免因为分辨率更改导致的文字模糊现象

#### 2. 代码控制
```C#
// 获取文本框组件
TMPro.TMP_Text tmp = this.GetComponent<TMPro.TMP_Text>();
// 更新文本值
tmp.text = "UGUI-TMP";
// 更新文字大小
tmp.fontSize = 16;
```

#### 3. 对于中文环境建议
1. 对于游戏中显示的文字可以使用 TextMesh Pro 的 SDF 字体，提高显示效果和特效处理；
2. 对于游戏中的输入框建议使用 UGUI 自带输入框，使用动态字体。

#### 4. 制作中文字体集
- 制作路径：Window>TextMeshPro>Font Asset Creator
##### 1. 素材
1. 下载一份中文 ttf 字体集
2. 下载一份常用汉字 txt 文档
##### 2. 参数选择
1. Source Font File：选择中文字体集
2. Character Set：
    1. Custom Characters：在原有的字体集上增加汉字，或者直接指定若干汉字
    2. Character from File：选择汉字文档
3. Character File：选择 2-4-1-2 中的汉字文档
4. 点击 Generate Font Atlas 生成中文字体集
5. 点击 Save as 存储到相应位置

### 3、Image

### 4、Raw Image

### 5、Button

### 6、Button-TextMeshPro

### 7、Toggle

### 8、Slider

### 9、Scrollbar

### 10、Dropdown

### 11、Dropdown-TextMeshPro

### 12、Input Field

### 13、Input Field-TextMeshPro

### 14、Canvas

### 15、Panel

### 16、Scroll View

### 17、Event System


