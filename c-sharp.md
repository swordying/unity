## C# 与 Unity

### 1、基本介绍
1. 目录：Project>Assets>Script
2. 文件名与类名必须一致

### 2、挂载脚本，Unity 物体关联脚本
1. 第一种：Add Component>Script
2. 直接把文件拖动到物体的组件列表中
- 备注：类名称的修改必须使用自带的重命名功能修改，才可以同步到引用文件的组件中

### 3、在创建 C# 文件时自动生成的初始代码
```C#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class First : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
```

### 4、找到物体
1. 当前物体 ` this.gameObject `
2. 当前物体的名字 ` this.gameObject.name `
3. 通过物体名称找到物体 ` GameObject target = GameObject.Find("物体名称或者全路径"); `
4. 在检查器中直接指定 物体 与 脚本的变量 绑定【相对 3 运行效率高】
5. 获取父级 ` Transform parent = this.transform.parent; `
6. 获取父级节点 ` GameObject parent_node = this.transform.parent.gameObject; `
7. 遍历子级 ` foreach(Transform child in this.transform){} `
8. 更换父级 ` this.transform.SetParent(this.transform.Find("物体的名称")); ` 或 ` this.transform.SetParent(GameObject.Find("物体的名称").transform); `
9. 显示隐藏
    ```C#
    if(this.gameObject.activeSelf){
        this.gameObject.SetActive(false);
    }else{
        this.gameObject.SetActive(true);
    }
    ```
10. 定向获取子级
    ```C#
    Transform parent = this.transform.Find("父节点");
    int child_count = blood.childCount;
    GameObject first_child = parent.GetChild(0).gameObject;
    GameObject last_child = parent.GetChild(child_count - 1).gameObject;
    ```

### 5、获取物体的坐标
1. 当前物体的 Transform 组件 ` this.gameObject.transform ` == ` this.transform `
2. 获取坐标
```C#
/// 世界坐标
Vector3 position = this.transform.position;
/// 本地坐标，常用
Vector3 local_position = this.transform.localPosition;
// position == (x,y,z) == (0,1,0)
// 获取 x,y,z 坐标
float x = local_position.x;
float y = local_position.y;
float z = local_position.z;
// Vector3 为三维向量
```
3. 设置坐标
```C#
// 设置新的 本地坐标，x,y,z 的值必须为 float 类型
this.transform.localPosition = new Vector(0,3,0);
```

### 6、帧更新

#### 1. 相关概念
1. Frame：一个游戏帧
2. FrameRate：帧率/刷新率
3. FPS：Frames Per Second，每秒更新多少帧

#### 2. 有关帧的方法
1. ` Start() `：第一帧更新之前调用
2. ` Update() `：每一帧更新时调用

#### 3. 设置帧率为 60
- ` Application.targetFrameRate = 60 `

#### 3. 其他相关方法
1. Time.time 游戏持续时间，float 值
2. Time.deltaTime 两个帧之间的时间差
- 备注：Unity 的帧更新时间间隔是不固定的

### 7、移动物体

#### 1. 移动
```C#
Vector3 local_position = this.transform.localPosition;
if(local_position.y >= -2)
{
    local_position.y = local_position.y - 0.01f;
    this.transform.localPosition = local_position;
}
// 物体沿着 自身的 y 轴 每一帧 向下移动 0.01
```

#### 2. 匀速移动
- 注意：上面移动不是匀速的，所以可以使用 Time.deltaTime 来进行匀速运动
```C#
float speed = 1f;
float distance = speed * Time.deltaTime;
Vector3 local_position = this.transform.localPosition;
if (local_position.y >= -2f)
{
    // 更改坐标的另一种方式，相对运动
    this.transform.Translate(0,-distance,0);
}
// 物体沿着 y 轴 每一帧 向下移动的距离为 帧间隔*速度
```

#### 3. 相对运动详解
```C#
this.transform.Translate(dx,dy,dz,space);
// Space.World：相对于 世界坐标系
// Space.Self：相对于本地坐标系，较为常用，更多用于 前后左右 移动
```

#### 4. 物体转向
1. 获取朝向目标，` GameObject target = GameObject.Find("朝向的目标"); `
2. LookAt 目标，` this.transform.LookAt(target.transform); `
3. 向 "前" 运动 ` this.transform.Translate(0,0,3, Space.Self); `

5. 使用向量计算来移动物体
```C#
Vector3 p1 = this.transform.position;
Vector3 p2 = target.transform.position;
Vector3 = p2 - p1;
float distance = p.magnitude;
if(distance > 0.3f){
    float move = speed * Time.deltaTime;
    this.transform.Translate(0,0,move, Space.Self);
}
```

### 8、物体旋转

#### 1. 欧拉角旋转
##### 1. 语法
```C#
// 欧拉角 Euler Angle
// 以世界坐标为轴旋转 x,y,z
transform.eulerAngles = new Vector(0,45,0);
// 以本地坐标为轴旋转 旋转 45 度
transform.localEulerAngles = new Vector(0,45,0);
```
##### 2. 匀速旋转
```C#
// 匀速旋转
float speed = 30;
Vector3 angles = this.transform.localEulerAngles;
angles.y = angles.y + speed * Time.deltaTime;
this.transform.localEulerAngles = angles;
```

#### 2. Rotate() 旋转

##### 1. 语法
```C#
// x,y,z
this.transform.Rotate(0,0.5f,0, Space.Self);
// 为 0 表示不旋转
// Space.Self 本地坐标
// Space.World 世界坐标
```

##### 2. 匀速旋转
```C#
// 匀速旋转
float speed = 30;
this.transform.Rotate(0,speed*Time.deltaTime,0, Space.Self);
```

### 9、常用事件回调函数

1. Awake() 初始化，仅执行一次，必会执行一次
2. Start() 启用脚本，仅执行一次
3. Update() 帧更新脚本，每一帧
4. OnEnable() 每当组件启用时调用
5. OnDisable() 每当组件禁用时调用
6. LateUpdate() 更新脚本后更新脚本，每一帧
7. FixedUupdate() 一帧中调用多次，通常为物理运算 
8. Destory() 调用则销毁对象
9. OnDestory() 销毁对象后调用
10. GetComponent() 找到其他的脚本
11. GameObject.Find() 找到其他的物体所形成的对象

#### 1. 回调函数的执行顺序

- 脚本的优先级：默认情况下相同
1. 各脚本的 Awake() 执行
2. 各脚本的 Start() 执行
3. 各脚本的 Update() 执行

#### 2. 执行其他脚本的方法

##### 1. 消息调用
```C#
// 获取物体
GameObject target = GameObject.Find("物体名称");
// 可以调用这个物体下所有脚本的 function_name 方法
target.SendMessage("function_name");
```

##### 2. 通过组件来调用
```C#
// 获取物体
GameObject target = GameObject.Find("物体名称");
// 获取物体下特定的脚本【脚本也是组件之一】
First first_script = target.GetComponent<First>();
// 可以调用脚本 First 的 function_name 方法
first_script.function_name();
```

### 10、脚本参数

#### 1、主控脚本
- 游戏的主控逻辑，其中定义了游戏的全局参数与设计

#### 2、添加脚本参数
```C#
public float speed = 0.5f;
// 在检查器的脚本设置中可以找到此参数
// 1. 参数必须使用 public 来修饰
// 2. 参数的名称，就是变量名称
// 3. 参数的默认值，就是变量的默认值
// 4. 参数的工具提示，可以使用 [Tooltip("这个是提示")] 指定==注解功能
// 5. 参数的值不能为 null
// 6. 如果参数的类型是引用类型、则在检查器中可以修改并会同步到程序中
```

#### 3、参数的保存
- 在运行模式下，所有的经过修改的参数需要通过如下方法保存
1. 在 Play Mode 下，组件 Copy Component
2. 在 Edit Mode 下，组件 Paste Component Values

### 11、游戏输入

- 游戏的输入，可以来自鼠标、键盘、触摸屏、游戏手柄等

### 12、组件的使用

#### 0. 特别注意
1. 特别注意：继承 UnityEngine.MonoBehaviour 的类，不允许使用 new 实例。
2. 可以通过 GetComponent() 来获取相关对象并使用。

#### 1. 获取组件
```C#
// 获取物体
GameObject audio_button = GameObject.Find("播放音乐");
// 获取物体的音频组件
AudioSource audio = audio_button.GetComponent<AudioSource>();
audio.Play();
// <> 表示泛型，即获取 <AudioSource> 类型的组件
```

#### 2. 引用脚本中的组件
- 脚本也是物体的一个组件，其名称为 类名
```C#
// 获取物体
GameObject target = GameObject.Find("物体名称");
// 获取物体下特定的脚本【脚本也是组件之一】
First first_script = target.GetComponent<First>();
// 可以调用脚本 First 的 function_name 方法
first_script.function_name();
```

#### 3. 更改材质球
##### 1. 情景 1
```C#
// 获取资源中 路径：/Project/Assets/Resources/ 定义好的材质球
Material red_mesh_renderer = Resources.Load("红色") as Material;
// Material red_mesh_renderer = Resources.Load<Material>("红色");
MeshRenderer mesh_renderer = GetComponent<MeshRenderer>();
mesh_renderer.material = red_mesh_renderer;
```
##### 2. 情景 2
```C#
GameObject floor = GameObject.Find("地板");
MeshRenderer floor_mesh_renderer = floor.GetComponent<MeshRenderer>();
Material floor_material = floor_mesh_renderer.materials[0];
MeshRenderer mesh_renderer = GetComponent<MeshRenderer>();
mesh_renderer.material = floor_material;
// 更改为所选物体的全部材质球
// mesh_renderer.materials = floor_mesh_renderer.materials;
```

### 13、定时器与计时器
1. ` this.Invoke("function_name", delay); ` 只调用一次
2. ` this.InvokeRepeating("function_name", delay, interval); ` 循环调用
3. ` this.IsInvoking("function_name"); ` 是否在调度中
4. ` this.CancelInvoke("function_name"); ` 取消调度、从调度队列中删除
5. ` this.CancelInvoke(); ` 取消所有调度

```C#
void Start()
{
    Debug.Log("定时器 开始：当前时间 " + Time.time);
    // 第 10 秒后仅执行一次
    this.Invoke("setTimeout", 10);

    Debug.Log("计时器 开始：当前时间 " + Time.time);
    // 第 0 秒启动，每隔 2 秒执行一次
    this.InvokeRepeating("setInterval", 0, 2);
}
// 计时器
void setInterval()
{
    Debug.Log("计时器 启动：当前时间 " + Time.time);
}
// 定时器
void setTimeout()
{
    Debug.Log("定时器 启动：当前时间 " + Time.time);
    // 取消计时器
    this.CancelInvoke("setInterval");
}
```

### 14、向量

#### 1. 向量的长度
```C#
Vector3 v = new Vector3(3,4,0);
float length = v.magnitude;
```

#### 2. 标准常量
1. Vector3.zero 即 (0,0,0)
2. Vector3.up 即 (0,1,0)
3. Vector3.right 即 (1,0,0)
4. Vector3.forward 即 (0,0,1)

#### 3. 向量运算
1. 路程长度，向量加法
2. 物体距离，向量减法
3. 物体放大，向量的标量乘法

#### 1. 动态创建实例
```C#
Prefab p = Resources.Load<Prefab>("预制体名称");
GameObject node = Object.Instantiate(p, parent);
// 销毁实例
Object.Destroy(node);
```

### 15、场景切换
- 切换后场景中的程序会重新开始
```C#
using UnityEngine.SceneManagement;
SceneManager.LoadScene("scene");
// 备注：需要把场景加入到场景列表中
// 路径：场景>File>Bulid Settings
```

### 16、游戏退出
```C#
using UnityEditor;
#if UNITY_EDITOR
// 在编辑器模式下退出
EditorApplication.isPlaying = false;
#else
// 在实际游戏中退出
Application.Quit();
#endif
```