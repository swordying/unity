## Unity 数据存储

### 1、PlayerPrefs
#### 1. API 简介
```C#
// 1. 存储 int 类型的值 void
PlayerPrefs.SetInt("int",1);
// 2. 存储 float 类型的值
PlayerPrefs.SetFloat("float",1.0f);
// 3. 存储 string 类型的值 void
PlayerPrefs.SetString("string","string");
// 4. 获取 int 类型的值 void
int get_int = PlayerPrefs.GetInt("int");
// 5. 获取 float 类型的值
float get_float = PlayerPrefs.GetFloat("float");
// 6. 获取 string 类型的值
string get_string = PlayerPrefs.GetString("string"); // string
// 7. 判断是否存在键
bool has_string = PlayerPrefs.HasKey("string"); // True
// 8. 删除键对应的值 void
PlayerPrefs.DeleteKey("string");
// 9. 删除所有的键值对 void
PlayerPrefs.DeleteAll();
```

#### 2. 备注
1. PlayerPrefs 为本地存储
2. 优点：简单便捷、拓展性强
3. 关闭程序后数据依然存在

### 2、JsonUtility
#### 1. API 简介
```C#
// 必须声明可序列化
[System.Serializable]
class Mobile
{
    public string name;
    public string system;
}
// 目标数据
Mobile mobile = new Mobile();
mobile.name = "Apple";
mobile.system = "iOS";
// 格式为 JSON
string json = JsonUtility.ToJson(mobile); // {"name":"Apple","system":"iOS"}
// 存储 JSON
string json_file_path = Application.dataPath + "/" + "Resources" + "/" + "mobile.json";
File.WriteAllText(json_file_path, json);
// 读取 JSON
string current_json = File.ReadAllText(json_file_path); // {"name":"Apple","system":"iOS"}
// 解析 JSON
Mobile current_mobile = JsonUtility.FromJson<Mobile>(current_json);
```

#### 2. 基于对象的数组转化为 JSON
```C#
// 必须声明可序列化
[System.Serializable]
class Mobiles
{
    public Mobile[] mobile;
}

// 目标数据
Mobiles mobiles = New Mobiles();

Mobile iphone = new Mobile();
iphone.name = "iPhone";
iphone.system = "iOS";

Mobile huawei = new Mobile();
huawei.name = "华为伙伴";
huawei.system = "鸿蒙";

int mobile_count = 2;

mobiles.mobile = new Mobile[mobile_count];

mobiles.mobile[0] = iphone;
mobiles.mobile[1] = huawei;

// 格式为 JSON
string mobiles_json = JsonUtility.ToJson(mobiles); // {"mobile":[{"name":"iPhone","system":"iOS"},{"name":"华为伙伴","system":"鸿蒙"}]}

// 存储 JSON
string json_file_path = Application.dataPath + "/" + "Resources" + "/" + "mobiles.json";
File.WriteAllText(json_file_path, mobiles_json);

// 读取 JSON
string current_mobiles_json = File.ReadAllText(json_file_path); // {"mobile":[{"name":"iPhone","system":"iOS"},{"name":"华为伙伴","system":"鸿蒙"}]}

// 解析 JSON
Mobiles current_mobiles = JsonUtility.FromJson<Mobiles>(current_mobiles_json);
// current_mobiles.mobile[1].system // 鸿蒙
```

#### 3. 备注
1. 持久化存储
2. 将数据压缩和解压缩为基于文本的格式。
3. 使用 JSON 序列化与 Web 服务进行交互
