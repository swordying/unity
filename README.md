# Unity 学习笔记

## 1、简介
1. 概念：一款 3D 游戏开发引擎
2. 用途：游戏开发、虚拟现实 VR、增强现实 AR
3. 优点：开发环境友好、工具套件丰富、跨平台

## 2、安装
1. 安装 Unity Hub，
2. 然后确定某个版本下载即可，
3. 如果是个人，可以免费申请个人许可证

## 3、创建项目
1. 打开 Unity Hub，点击创建即可
2. 注意：如果不需要版本管理，可以不安装
3. 注意：创建项目需要登录与申请个人许可证

## 4、Unity 界面与控件简介
- 建议使用英文界面

### 1. Global 与 Local
1. Global 绝对坐标系、世界坐标
    1. 以世界中心为轴
    2. 6个方向代表：上下 东西 南北
2. Local 本地坐标系、相对坐标
    1. 以物体自身为轴
    2. 6个方向代表：上下、前后、左右
3. y轴成为 up，z轴成为 forward，x轴称为 right，一般默认模型的正面与 z 轴方向一致

### 2. 物体操作的工具
1. Move Tool 移动工具
2. Rotate Tool 旋转工具
3. Scale Tool 缩放工具
4. Pivot 轴心 / Center 中心点 
    1. Pivot 轴心，指一个物体操作的基准点，坐标默认原点，用于旋转、缩放，只能在建模软件中修改
    2. Center 几何中心
    3. 一般来说，物体的轴心并不在几何中心处

### 3. Hierarchy 层级窗口
- 显示参与程序的各种对象，即节点管理窗口

#### 1. 初始化后有一个默认场景 SampleScens
1. Main Camera 主摄像机
2. Directional Light 平行光

#### 2. 添加物体 3D Object > 点击鼠标右键
1. Cube 立方体
2. Sphere 球体
3. Capsule 胶囊体
4. Cylinder 圆柱体
5. Plane 平面
    1. 在 Unity 中 一个平面是没有厚度的
    2. 平面的正面可见、背面透明（即不会被渲染）
6. Quad 四方体
7. Text-TextMeshPro 文本网格
8. Ragdoll 岩石雕塑
9. Terrain 地形
10. Tree
11. Wind Zone 风带
12. 3D Text

#### 3. 父子节点
1. 父子节点的意思：可以进行同步操作，
2. 子节点的原点坐标是父节点的中心坐标

#### 4. 空物体 EmptyObject，即空对象、空节点
1. 用于节点的组织与管理
2. 用于标记一个位置
3. 空间标识

### 4. Scene 场景窗口，3D 视图窗口
- 游戏运行的视图，或者是游戏中的一处关卡
- x-红轴 y-绿轴 z-蓝轴

#### 1. 导航器 Gizmo
- 表示世界坐标的方向
1. 点击 Y，俯视图
2. 点击 X，右视图
3. 点击 Z，前视图
4. 点击中间，恢复方向

#### 2. 栅格 Grid
- 表示 XZ 坐标的平面

#### 3. 天空盒 Skybox
- 表示游戏世界的背景

#### 4. 视图操作
1. 旋转视图
2. 缩放视图
3. 平移视图

#### 5. 透视与正交
- 在导航器下
1. 透视视图 Perspective，近大远小
2. 正交视图 Orthographic，又称等距视图 Isometric
3. 透视畸变与广角设定，Scene Camera > Field of View 可以设为 30-40度

#### 6. Camera 摄像头

##### 1. Align With View 3D 视图对齐
1. 使摄像头拍摄的 Game 画面与观察者在 Scene 中看到的一致
2. 方法：选中摄像机，调整 GameObject.Align With View

### 5. Game 游戏播放窗口
- 摄像头拍摄的画面

### 6. Inspector 检查器窗口，属性窗口
- 场景窗口中物体的 组建列表、属性列表

#### 1. Transform 物体的坐标信息
- 所有物体必有的一个组件，不能被删除
1. Position 物体的坐标
2. 如果物体没有父节点，其坐标为世界坐标

#### 2. Cube Mesh Filter
- 网格过滤器，加载网格数据

#### 3. Mesh Renderer
- 网格渲染器，渲染网格数据

#### 4. Materials 材质
##### 1. 更改物体表面材质的颜色
- 关键字：Albedo 反照率
1. 在 Assets 中新建目录，
2. 在目录中 create创建 一个 Meterial材质，
3. 调整 材质 Inspector>Main Maps>Albedo
4. 直接把 调整好的 模版材质 拖到指定物体即可

#### 5. Lighting 光源

#### 6. Probes

#### 7. Additional Settings

#### 8. Box Collider 碰撞框

#### 9. Audio Source 音频组件
1. 添加一个音频文件，
2. 创建一个物体，
3. 文件与物体的 AudioSource.AudioClip 绑定
4. 在 3D 窗口上方，选择 Toggle Audio On 播放

##### Trigger 接触器
1. OnTriggerEnter 两个物体开始碰撞时调用
2. OnTriggerStay 两个物体持续碰撞时调用
3. OnTriggerExit 两个结束碰撞时调用

##### Collision 碰撞器

##### Rigidbody
1. Mass 质量
2. Drag 阻力
3. Angular Drag 角速度阻力
4. Use Gravity 重力
5. Is Kinematic 运动学交互与物理学交互转换
6. Interpolate 防抖动差值
7. Collision Detection 检测两个物体是否在同一帧中
8. Constraints 是否阻止物理学运动干预物理

#### 10. Shader

### 7. Project 项目窗口

#### 1. Assets 资源窗口

##### 1. 常见的资源类型
1. 模型文件 Model .fbx
2. 图片文件 Texture .jpg/png/psd/tif
3. 音频文件 AudioClip .mp3/wav/aiff
4. 脚本文件 Script .cs
5. 材质文件 Material *.mat
6. 场景文件 Scene *.unity
7. Meta 文件，系统自动生成的，无需管理，不能删除

##### 2. 资源包
1. 后缀名 .unitypackage
2. 导出：选择点击鼠标右键，选择 Export Package
3. 导入：直接把资源包拖动到 Assets 目录
4. 可以使用 Unity 资源商店 的资源包

##### 3. Assets 脚本 Scripts
- 注意：文件名与类名必须一致
1. 新建 C# 脚本
2. 编写代码，在 Visual Studio 中编辑
3. 挂载脚本，Unity 物体关联脚本
    1. 第一种：Add COmponent>Script
    2. 直接把文件拖动到物体的组件列表中
- 备注：类名称的修改必须使用自带的重命名功能修改，才可以同步到引用文件的组件中


### 8. Console 控制台窗口
1. 打印信息
2. 调试信息

## 5、其他

### 1. 网格 Mesh

#### 1. 基本概念
1. Mesh 存储了模型的形状（面和顶点）数据
2. 模型的形状，由若干个小面围合而成
3. 模型是中空的
4. Mesh 中包含了 面、顶点坐标、面的法向 等数据

#### 2. Unity 中观察模型的网格
- 任何物体的表面都是由若干 三角面 围成
1. Shaded 着色模式，显示表面材质
2. Wireframe 线框模式，仅显示网格
3. Shaded Wireframe 线框着色模式

### 2. 材质 Material

#### 1. 基本概念
1. Material 定义了物体的表面细节
2. 颜色、金属/非金属、光滑/粗糙、透明度、凹陷/凸起

### 3. 纹理 Texture，也称贴图
- 用一张图来定义物体的表面颜色

#### 1. Unity 中使用
1. 目录：Assets>Textures
2. 在目录中添加相关的贴图
3. 更改 Inspector>Main Maps>Albedo 属性
4. 直接拖动即可

### 4. 外部模型 fbx 文件

#### 1. 基本简介
1. Unity 支持各种 CG 建模软件：3D Max/Maya/Blender/Cinema4D/ZBrush
2. 标准模型格式： .fbx
3. fbx 文件包换了：网格、贴图、材质

#### 2. 导入 Unity 步骤
1. 在 Assets 中添加外部模型目录
2. 直接把 FBX 文件拖到目录中即可
3. 导入后直接用鼠标拖到场景或层级窗口中即可
4. 更改外部模型的材质：
    - 重映射：直接拖动另外的材质到 fbx 预设
    - 使用外部材质 Location 设置为 Use External Materials
    - 分解重组，单独拿出网格，然后设置材质

### 5. Prfabs 
- 一簇模型的母版，修改母版可以动态更新到相关模型

### 6. Scenes In Build
- 根据目标平台打包不用的包


## 6、Unity 中 C# 脚本的运用

### 1. Instantiate() 实例
1. Awake() 启动脚本，场景启动
2. Start() 开始脚本
3. Update() 更新脚本，每一帧
4. LateUpdate() 更新脚本后更新脚本，每一帧
5. FixedUupdate() 一帧中调用多次，通常为物理运算 
6. Destory() 调用则销毁对象
7. OnDestory() 销毁对象后调用
8. GetComponent() 找到其他的脚本
9. GameObject.Find() 找到其他的物体所形成的对象
10. OnEnable() 物体设置 Active 时触发

### 2. 游戏输入
#### 1. Edit>Project Settings>Input 设置游戏输入
1. 在脚本中利用 Input 类来检测输入状态
2. Input.GetAxis 返回的值是 -1 到 1 之间，0 表示没有输入
3. Input.GetMouseButtonUp(); 检测鼠标按键是否抬起

### 3. 动画 Animation
1. 状态机 motion 增加状态

### 4. 动态生成 与 销毁物体

### 5. 自动寻路
1. 设置导航网格
2. 设置 Nav Mesh Agent
3. 调用 Nav Mesh Agent 的方法设置路径

## 7、物理系统 Physics

### 1. 刚体组件 Rigidbody

### 2. 碰撞体 Collider
1. Box Collider 长方碰撞体
2. Sphere Collider 球形碰撞体

### 3. 物理材质 Physic Material
1. Friction 摩擦系数
2. Bounciness 反弹系数

### 4. 碰撞检测

## 8、天空盒 Skybox
1. 光照设置 Lighting
2. 天空盒材质 Skybox Material

## 9、粒子特效 Particle System

## 10、预制体
- 编辑物体后，直接拖拽到 Assets 目录中即可

## 11、Unity 相关路径
### 1. Application.streamingAssetsPath
1. 流式资源文件夹，于存放游戏持久化资源文件的
2. 应用：游戏文本资源，游戏词库，ab包
3. 路径：项目目录/Assets/StreamingAssets
4. 注意：游戏运行期间此目录只可读取不可写入

### 2. Application.persistentDataPath
1. 持久化资源文件夹，
2. 应用：游戏存档

## 附录其他
1. Unity 中国官方 https://unity.cn/
2. 中文文档 https://docs.unity.cn/
3. 相关术语 https://docs.unity.cn/cn/2019.3/Manual/Glossary.html
